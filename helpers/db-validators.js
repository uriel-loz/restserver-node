const Role = require('../models/role');
const User = require('../models/user');

const isRoleValidate = async ( role = '' ) => {

    const existRole = await Role.findOne( { role } );

    if ( !existRole ) 
        throw new Error( `The role ${ role } is not registered in the database` );
}

const emailExists = async ( email = '' ) => {

    const findEmail = await User.findOne( { email } );

    if ( findEmail ) 
        throw new Error( `Email: ${email} already exists` );
}

const existsUserById = async ( id ) => {

    const existsUser = await User.findById( id );

    if ( !existsUser ) 
        throw new Error( `The id no exists: ${ id } in the database` );
}

module.exports = {
    isRoleValidate,
    emailExists,
    existsUserById
}