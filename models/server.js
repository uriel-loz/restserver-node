const express = require( 'express' );
const cors = require('cors');

const fileUpload = require('express-fileupload');

const { connectionDB } = require('../database/config');

class Server {
    constructor() {
        this.app = express();
        
        this.port = process.env.PORT || 3000;

        // Connect to database
        this.connectDB();

        // Middlewares
        this.middlewares();

        // Routes
        this.routes();
    }

    async connectDB() {
        await connectionDB();
    }

    middlewares() {
        // CORS
        this.app.use( cors() );

        // Parse and read body
        this.app.use( express.json() );

        // Public directory
        this.app.use( express.static( 'public' ) );

        // File upload
        this.app.use( fileUpload( {
            useTempFiles : true,
            tempFileDir : '/tmp/'
        } ) );
    }

    routes() {
        this.app.use( '/api/auth', require( '../routes/auth' ) );
        this.app.use( '/api/products', require( '../routes/product' ) );
        this.app.use( '/api/categories', require( '../routes/category' ) );
        this.app.use( '/api/search', require( '../routes/search' ) );
        this.app.use( '/api/uploads', require( '../routes/upload' ) );
        this.app.use( '/api/users', require( '../routes/user' ) );
    }

    listen() {
        this.app.listen( this.port, () => {
            console.log( 'Server running on port', this.port );
        } )
    }
}

module.exports = Server;
