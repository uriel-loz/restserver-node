const { Router } = require('express');
const { check } = require('express-validator');

const { validateFields, validateJWT, checkRole } = require( '../middlewares' );

const { isRoleValidate, emailExists, existsUserById } = require('../helpers/db-validators');
const { index, store, update, destroy } = require('../controllers/user-controller');


const router = Router();

router.get( '/', index );

router.post( '/', [
    check( 'name', 'The name is required' ).not().isEmpty(),
    check( 'password', 'The password is required' ).not().isEmpty(),
    check( 'email', 'Invalid email' ).isEmail().custom( emailExists ),
    check( 'role' ).custom( isRoleValidate ),
    // check( 'role', 'Invalid role' ).isIn( [ 'ADMIN_ROLE', 'USER_ROLE' ] ),
    validateFields
], store );

router.put( '/:id', [
    check( 'id', 'Invalid id' ).isMongoId().custom( existsUserById ),
    check( 'role' ).custom( isRoleValidate ),
    validateFields
], update );

router.delete( '/:id', [
    validateJWT,
    checkRole( 'ADMIN_ROLE', 'USER_ROLE' ),
    check( 'id', 'Invalid id' ).isMongoId().custom( existsUserById ),
    validateFields
], destroy );

module.exports = router;