const { Router } = require('express');
const { check } = require('express-validator');

const { validateFields, validateJWT, checkRole, explicitBinding,   } = require( '../middlewares' );
const { index, store, show, update, destroy } = require('../controllers/category-controller');

const router = Router();

router.get( '/', index );

router.post( '/', [
    validateJWT,
    check( 'name', 'The name is required' ).not().isEmpty(),
    validateFields
], store );

router.get( '/:category', [
    check( 'category', 'Invalid id' ).isMongoId(),
    explicitBinding( 'category' ),
    validateFields
], show );

router.put( '/:category',[
    validateJWT,
    check( 'name', 'The name is required' ).not().isEmpty(),
    explicitBinding( 'category' ),
    validateFields
], update );

router.delete( '/:category', [
    validateJWT,
    checkRole( 'ADMIN_ROLE', 'USER_ROLE' ),
    check( 'category', 'Invalid id' ).isMongoId(),
    explicitBinding( 'category' ),
    validateFields
], destroy );


module.exports = router;