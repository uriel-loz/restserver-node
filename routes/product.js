const { Router } = require('express');
const { check } = require('express-validator');

const { validateFields, validateJWT, checkRole, explicitBinding } = require( '../middlewares' );
const { index, store, show, update, destroy } = require('../controllers/product-controller');

const router = Router();

router.get( '/', index );

router.post( '/', [
    validateJWT,
    check( 'name', 'The name is required' ).not().isEmpty(),
    check( 'category', 'The category is required' ).not().isEmpty().isMongoId().withMessage( 'Invalid id' ),
    validateFields
], store );

router.get( '/:product', [
    check( 'product', 'Invalid id' ).isMongoId(),
    explicitBinding( 'product' ),
    validateFields
], show );

router.put( '/:product',[
    validateJWT,
    check( 'product', 'The name is required' ).not().isEmpty(),
    explicitBinding( 'product' ),
    validateFields
], update );

router.delete( '/:product', [
    validateJWT,
    checkRole( 'ADMIN_ROLE', 'USER_ROLE' ),
    check( 'product', 'Invalid id' ).isMongoId(),
    explicitBinding( 'product' ),
    validateFields
], destroy );


module.exports = router;