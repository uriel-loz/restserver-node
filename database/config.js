const mongoose = require('mongoose');

const connectionDB = async () => {
    try{
        await mongoose.connect( process.env.MONGODB_URI );

        console.log('Database online');
    } catch (error) {
        console.log(error);
        throw new Error('Error starting database');
    }
}

module.exports = {
    connectionDB
};