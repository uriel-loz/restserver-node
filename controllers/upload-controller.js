const path = require('path');
const { v4: uuidv4 } = require('uuid');

const { response } = require("express")

const uploadFile = ( req, res = response ) => {

    if ( !req.files || Object.keys( req.files ).length === 0 || !req.files.file ) 
        return res.status( 400 ).json( { message: 'No files were uploaded.' } );

    const { file } = req.files;
    const name = file.name.split( '.' );
    const extension = name[ name.length - 1 ];

    const validExtensions = [ 'png', 'jpg', 'jpeg', 'gif' ];

    if ( !validExtensions.includes( extension ) ) {
        return res.status( 400 ).json( { 
            message: `Invalid extension. Valid extensions: ${ validExtensions }` 
        } );
    } // end if

    const tempName = uuidv4() + '.' + extension;
    const uploadPath = path.join( __dirname , '../uploads/' , tempName ) ;

    // Use the mv() method to place the file somewhere on your server
    file.mv( uploadPath, ( err ) => {
        if ( err )
            return res.status(500).json( { err } );

        res.json( { message: 'File uploaded!' } );
    });
}

module.exports = {
    uploadFile
}