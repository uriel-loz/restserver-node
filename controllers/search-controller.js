const { response } = require("express");
const { User, Category, Product } = require("../models");
const { ObjectId } = require( 'mongoose' ).Types;

const collectionsAllowed = [
    'users',
    'categories',
    'products',
    'roles'
];

const searchUser = async( term = '', res = response ) => {
    const isMongoID = ObjectId.isValid( term );

    if ( isMongoID ) {
        const user = await User.findById( term );

        return res.status( 200 ).json( {
            'code': 200,
            'status': 'success',
            'message': 'Operation Successfull',
            'data': ( user ) ? [ user ] : []
        } );
    }

    const regex = new RegExp( term, 'i' ); // insensitive

    const users = await User.find( { 
        $or: [ { name: regex }, { email: regex } ],
        $and: [ { status: true } ]
    } );
    // .where( { status: true } );

    res.status( 200 ).json( {
        'code': 200,
        'status': 'success',
        'message': 'Operation Successfull',
        'data': users
    } );
}

const searchCategory = async( term = '', res = response ) => {
    const isMongoID = ObjectId.isValid( term );

    if ( isMongoID ) {
        const category = await Category.findById( term );

        return res.status( 200 ).json( {
            'code': 200,
            'status': 'success',
            'message': 'Operation Successfull',
            'data': ( category ) ? [ category ] : []
        } );
    }

    const regex = new RegExp( term, 'i' ); // insensitive

    const categories = await Category.find( { name: regex, status: true } );

    res.status( 200 ).json( {
        'code': 200,
        'status': 'success',
        'message': 'Operation Successfull',
        'data': categories
    } );
}

const searchProduct = async( term = '', res = response ) => {
    const isMongoID = ObjectId.isValid( term );

    if ( isMongoID ) {
        const product = await Product.findById( term ).populate( 'category', 'name' );

        return res.status( 200 ).json( {
            'code': 200,
            'status': 'success',
            'message': 'Operation Successfull',
            'data': ( product ) ? [ product ] : []
        } );
    }

    const regex = new RegExp( term, 'i' ); // insensitive

    const products = await Product.find( { name: regex, status: true } ).populate( 'category', 'name' );

    res.status( 200 ).json( {
        'code': 200,
        'status': 'success',
        'message': 'Operation Successfull',
        'data': products
    } );
}

const search = ( req, res = response ) => {
    const { collection, term } = req.params;

    if ( !collectionsAllowed.includes( collection ) ) {
        return res.status( 400 ).json({
            message: `The collections allowed are: ${ collectionsAllowed }`
        });
    }

    const executeFunctionByCollection = {
        users: () => searchUser( term, res ),
        categories: () => searchCategory( term, res ),
        products: () => searchProduct( term, res ),
    };

    const executeSearch = executeFunctionByCollection[ collection ];

    if ( executeSearch ) {
        executeSearch();
    } else {
        res.status( 500 ).json( {
            message: 'Collection not found',
        } );
    }
}

module.exports = {
    search
}