const { response, request } = require("express");

const bcrypt = require('bcrypt');

const User = require("../models/user");
const { generateJWT } = require("../helpers/generate-jwt");

const login = async( req, res = response ) => {
    const { email, password } = req.body;

    try {

        const user = await User.findOne( { email } );

        if ( !user ) {
            return res.status( 400 ).json( {
                code: 400,
                msg: 'User / Password are not correct - email'
            } );
        }

        if ( !user.status ) { 
            return res.status( 400 ).json( {
                code: 400,
                msg: 'User deleted - status: false'
            } );
        }

        const validPassword = bcrypt.compareSync( password, user.password );

        if ( !validPassword ) {
            return res.status( 400 ).json( {
                code: 400,
                msg: 'User / Password are not correct - password'
            } );
        }

        const token = await generateJWT( user.id );

        res.status( 200 ).json( {
            code: 200,
            user,
            token
        } );
        
    } catch (error) {
        console.log( error );

        res.status( 500 ).json( {
            code: 500,
            msg: 'Something went wrong'
        } );
    }
}

module.exports = {
    login
}