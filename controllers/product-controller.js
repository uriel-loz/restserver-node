const { response } = require("express");
const { Product, Category } = require("../models");

const index = async( req, res = response ) => {
    const { from = 0, limit = 5 } = req.query;
    const status = { status: true };

    const queryProducts = Product.find( status )
                                    .populate( 'user', 'name' )
                                    .populate( 'category', 'name' )
                                    .skip( from )
                                    .limit( limit );

    const queryTotal = Product.countDocuments( status );


    const [ products, total ] = await Promise.all( [ queryProducts, queryTotal ] );

    const output = {
        code: 200,
        status: 'success',
        message: 'Success Operation',
        data: {
            total,
            products
        }
    }

    res.status( 200 ).json( output );
}

const store = async( req, res = response ) => {
    let output = {
        code: 500,
        status: 'error'
    }

    try {
        
        let data = req.body;
        data.user = req.uid;

        data.category = await Category.findById( data.category );

        if ( !data.category ) 
            throw { message: "Category not found", code: 404 };
        
        await Product.create( data );

        output = {
            code: 200,
            status: 'success',
            message: 'Operation Successfull'
        }

        res.status(200).json(
            output
        );

    } catch (error) {
        console.log( error );

        output.code  = error.code;
        output.message = error.message;

        res.status( output.code ).json(
            output
        );
    }
}

const show = ( req, res = response ) => {
    const product = req.product;

    const output = {
        code: 200,
        status: 'success',
        message: 'Operation Successfull',
        data: product
    }

    res.status( output.code ).json(
        output
    );
}

const update = async( req, res = response ) => {
    let output = {
        code: 500,
        status: 'error'
    }

    try {
        
        let data = req.body;
        data.user = req.uid;

        data.category = await Category.findById( data.category );

        if ( !data.category ) 
            throw { message: "Category not found", code: 404 };
        
        const product = await Product.findByIdAndUpdate( req.product._id, data, { new: true } );

        output = {
            code: 200,
            status: 'success',
            message: 'Operation Successfull',
            data: product
        }

        res.status(200).json(
            output
        );

    } catch (error) {
        console.log( error );

        output.code  = error.code;
        output.message = error.message;

        res.status( output.code ).json(
            output
        );
    }
}

const destroy = async( req, res = response ) => {
    const product = await Product.findByIdAndUpdate( req.product._id, 
                            { 
                                status:false, 
                                deletedAt: Date.now(), 
                                avalible: false 
                            }, 
                            { 
                                new: true 
                            } );

    output = {
        code: 200,
        status: 'success',
        message: 'Operation Successfull',
        data: product
    }

    res.status(200).json(
        output
    );
}

module.exports = {
    index,
    store,
    show,
    update,
    destroy
}