const { response } = require("express");
const { Category } = require('../models');


const index = async( req, res = response ) => {
    const { from = 0, limit = 5 } = req.query;
    const status = { status: true };

    const queryCategories = Category.find( status )
                                .populate( 'user', 'name' )
                                .skip( from )
                                .limit( limit );

    const totalCategories = Category.countDocuments( status );

    const [ categories, total ] = await Promise.all( [ queryCategories, totalCategories ] );

    const output = {
        code: 200,
        status: 'success',
        data: {
            categories,
            total
        }
    };

    return res.status( 200 ).json( output );

}

const store = async( req, res = response ) => {
    try {
        const name = req.body.name.toUpperCase();

        const categoryFound = await Category.findOne( { name } );

        if ( categoryFound ) {
            return res.status( 400 ).json({
                msg: `The category ${ name } it already exists`
            });
        }

        const data = {
            name,
            user: req.uid,
        }

        const category = new Category( data );
        await category.save();

        res.status( 201 ).json( category );
    } catch ( error ) {
        console.log( error );

        res.status( 500 ).json( {
            code: 500,
            msg: 'Something went wrong'
        } );
    }
}

const show = async( req, res = response ) => {
    const category = req.category;

    return res.status(200).json({
        code: 200,
        category
    });
   
}

const update = async( req, res = response ) => {
    const  name  = req.body.name.toUpperCase();

    const category = await Category.findByIdAndUpdate( req.category._id, { name }, { new: true } );

    return res.status(200).json({
        code: 200,
        category
    });
}

const destroy = async( req, res = response ) => {
    const category = await Category.findByIdAndUpdate( req.category._id, { status: false, deletedAt: Date.now() }, { new: true } );

    return res.status(200).json({
        code: 200,
        category
    });
}

module.exports = {
    index,
    store,
    show,
    update,
    destroy
}