const { response, request } = require('express');

const bcrypt = require('bcrypt');

const User = require('../models/user');

const index = async( req = request, res = response ) => {

    const { from = 0, limit = 5 } = req.query;
    const activeStatus = { status: true };

    const queryUsers = User.find( activeStatus )
                            .skip( from )
                            .limit( limit );

    const queryTotal = User.countDocuments( activeStatus );

    const [ total, users ] = await Promise.all( [ queryTotal, queryUsers ] );

    res.status( 200 ).json( {
        code: 200,
        total,
        users
    } );
}

const store = async( req, res = response ) => {
    const { name, email, password, role } = req.body;
    const user = new User( { name, email, password, role } );

    // Encrypt password
    const salt = bcrypt.genSaltSync();
    user.password = bcrypt.hashSync( password , salt);

    await user.save();

    res.status( 200 ).json( {
        code: 200,
        user
    } );
}

const update = async( req, res = response ) => {
    const { id } = req.params;
    let { _id, password, google, ...rest } = req.body;

    if ( password ) {
        const salt = bcrypt.genSaltSync();
        rest.password = bcrypt.hashSync( password , salt);
    } // end if

    const user = await User.findByIdAndUpdate( id, rest, { returnDocument: 'after' } );

    res.status( 201 ).json( {
        code: 200,
        user
    } );
}

const destroy = async( req, res = response ) => {
    const { id } = req.params;

    // const uid = req.uid;
    const {user} = req;

    // hard delete
    // const user = await User.findByIdAndDelete( id );

    const userDelete = await User.findByIdAndUpdate( id, { status: false, deletedAt: Date.now() }, { returnDocument: 'after' } );

    res.status( 201 ).json( {
        code: 200,
        user,
        userDelete
    } );
}

module.exports = {
    index,
    store,
    update,
    destroy
}