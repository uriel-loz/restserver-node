const { response, request } = require('express');

const { Category,
        Product,
        Role,
        User } = require( '../models' );

const SELECT_MODEL = {
    category: Category,
    product: Product,
    role: Role,
    user: User
};

// const toCamelCase = (str) => {
//     return str.replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
//       return word.toUpperCase();
//     } ).replace(/\s+/g, '');
// }

const explicitBinding = ( modelParam ) => {
    return async( req, res = response, next ) => {
        const params = req.params;
        // const Model = toCamelCase(modelParam);

        try {
            if (!SELECT_MODEL[ modelParam ]) {
                return res.status( 404 ).json({
                    code: 404,
                    msg: `Model not found ${ modelParam }`
                });
            } // end if

            const result = await SELECT_MODEL[ modelParam ]
                                    .findById( params[ modelParam ] )
                                    .where( { status: true } );

            if ( !result ) {
                return res.status( 404 ).json( {
                    code: 404,
                    msg: `Register from ${ modelParam } not found`
                } );
            }// end if
            
            req[ modelParam ] = result;

            next();

        } catch (err) {
            next(err);
        }
    }
    
}


module.exports = {
    explicitBinding
};