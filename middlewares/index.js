const checkRole = require('../middlewares/validate-roles');
const explictBinding = require( '../middlewares/explicit-binding' );
const validateFields = require('../middlewares/validate-fields');
const validateJWT = require('../middlewares/validate-jwt');

module.exports = {
    ...checkRole,
    ...explictBinding,
    ...validateFields,
    ...validateJWT,
}