const { response } = require('express');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

const validateJWT = async( req, res = response, next ) => {
    const token = req.header( 'x-token' );

    if ( !token ) {
        return res.status( 401 ).json( {
            code: 401,
            msg: 'Token is required'
        } );
    } // end if

    try {
        const { uid } = jwt.verify( token, process.env.SECRETORPRIVATEKEY );

        req.user = await User.findById( uid );

        if ( !req.user ) {
            return res.status( 401 ).json( {
                code: 401,
                msg: 'invalid token - user deleted'
            } );
        } // end if

        if ( !req.user.status ) {
            return res.status( 401 ).json( {
                code: 401,
                msg: 'invalid token - user deleted status false'
            } );
        } // end if

        req.uid = uid;

        next();
        
    } catch ( error ) {
        console.log( error );

        res.status( 401 ).json({
            code: 401,
            msg: 'Invalid token'
        });
    }
}

module.exports = {
    validateJWT
}